const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const cors = require('cors');
const Premier = require('./Schema')

const app = express()

app.use(cors());
app.use(bodyparser.json());

mongoose.connect('mongodb://localhost:27017/test', { useMongoClient: true });

mongoose.connection.on('connected', ()=>{
  console.log('Bonne connection a la DB')
});
mongoose.connection.on('error', (err)=>{
  if(err){
    console.log('Mauvaise connection a la DB => '+ err)
  }
});

app.get('/premier' , (req,res) => {

  Premier.find((err, Premier) => {
    if (err) {
      res.send(err)
    }
    res.json(Premier)
  })
})

app.post('/new', function(req, res) {
  let premier = new Premier()
  premier.newPremier = req.body.newPremier;

  premier.save(function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'Premier cree' });
  });
});

app.listen(process.env.PORT || 8081)