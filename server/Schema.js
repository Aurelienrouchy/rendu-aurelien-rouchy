let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let PremierSchema = new Schema({
  newPremier: String
});

module.exports = mongoose.model('Premier', PremierSchema)