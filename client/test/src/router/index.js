import Vue from 'vue'
import Router from 'vue-router'
import Home from './../components/Home'
import Prime from './../components/Prime'
import Music from './../components/Music'
import I18n from './../components/I18n'
import Graph from './../components/Graph'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/Prime',
      name: 'Prime',
      component: Prime
    },
    {
      path: '/Music',
      name: 'Music',
      component: Music
    },
    {
      path: '/I18n',
      name: 'I18n',
      component: I18n
    },
    {
      path: '/Graph',
      name: 'Graph',
      component: Graph
    }
  ]
})
