# Welcome to CityTaps JS test

This test aims at evaluating (quickly) your development skills with JS. There is no good or bad answer, this is rather a way to see how you write code.

This exercice opened to your creativity.  
Please limit yourself to 2 hours on this test, even if you do not achieve all the exercices.

## Requirements
* You will need to use Node for the backend and Angular or React for the front.
* Your answers should be push to this repo.
* Your answers should demonstrate your knowledge and practices.

## Evaluation
Your code will be tested on a localhost environment.  
Please document any installation requirements and how to launch your app.


## Your job
### 1. Customize the repo
Add an whoami.txt file with your name and the current date, commit it and push it.

### 2. Create your application
You have to create a web application with 4 pages : /music, /prime, /i18n and /graph, which content is explained below.

### Music
Let's start easy, tell us what are the 4 songs you want to add to the office playlist.

### Prime number
Now, some algo.
This exercice is about finding the n-th prime number.  

The page should ask for an integer n and display the n-th prime number. For instance, the 10-th prime is 29.  

The page should also list the 10 last requests from any client with their results, so Alice and Bob share the same history.

The algorithm to determine if a number is prime or not can be implemented the server or the client side but not rely on an external service. Please explain the option you chose.

**Bonus**: find a way to store the request history permanently on the server.

### i18n
We have a translation JSON that looks like:  
```json
{
  "dashboard": {
    "money": {
      "fr": "Montant collecté",
      "en": "Collected cash"
    },
    "water": {
      "waterConsummed": {
        "fr": "Volume consommé",
        "en": "Used volume of water"
      },
      "waterAvailable": {
        "fr": "Volume disponible",
        "en": "Available of water"
      }
    }
  },
  "customer": {
    "company": {
      "fr": "société",
      "en": "company"
    },
    "country": {
      "fr": "pays",
      "en": "country"
    }
  },
  "user": {
    "name": {
      "fr": "nom",
      "en": "name"
    }
  }
}
```

Write an algorithm that return the value of a key, where key is provide as a string, for example "dashboard.water.waterAvailable.fr" or "user.name.en".  
The function signature should be `i18n(translationJSON, key)`` and return a string.

### Graph

We want to display collected data as graph, to see the evolution of some metrics.  
You will find a CSV file in the repo with a set of data.

Create a graph using the data to display the evolution of debit, redit and balance over time.

## 3. Notify me
Send me an email once your done so I can check your work.



# Thank you for your time and interest in CityTaps !
